import itertools
import random
# from math import exp

import pokeapi_communication

# TODO : one type by Pokemon atm


def flatten(list_of_lists):
    return list(itertools.chain.from_iterable(list_of_lists))


def fitness(individual):
    types_covered_by_team = map(pokeapi_communication.get_types_covered_for_type, individual)
    types_covered_by_team = set(flatten(list(types_covered_by_team)))
    return len(types_covered_by_team)   # TODO : exp ?


def cross_over(parent_a, parent_b):
    # TODO : improve cross over to avoid duplicates
    midpoint = team_size // 2
    return parent_a[:midpoint] + parent_b[midpoint:]


def mutate(individual):
    # change type by another type not present in the team
    types_not_in_team = list(set(pokemon_types) - set(individual))
    individual[random.randrange(len(individual))] = random.choice(types_not_in_team)
    return individual


population_size = 30
team_size = 6
pokemon_types = pokeapi_communication.get_all_types()
population = [random.sample(pokemon_types, k=team_size, ) for x in range(population_size)]
iterations_count = 0
max_iterations = 100
mutation_rate = 0.8
best = None

while iterations_count < max_iterations or fitness(best) == len(pokemon_types):
    new_population = []
    for i in range(population_size):
        # we choose 2 parents with weights equals to scores
        parents = random.choices(population, weights=map(fitness, population), k=2)
        child = cross_over(parents[0], parents[1])
        if random.random() < mutation_rate:
            child = mutate(child)
        new_population.append(child)
    population = new_population
    iterations_count += 1
    best = max(population, key=fitness)

print(f"Best team : {best} after {iterations_count} generations, {fitness(best)} types covered")
