import requests
import json

base_api_url = "https://pokeapi.co/api/v2/"


def get_all_types():
    api_response = requests.get(base_api_url + "type")

    # "unknown" and "shadow" are not really common types
    return [result["name"] for result in api_response.json()["results"]
                              if result["name"] != "unknown" and result["name"] != "shadow"]


def retrieve_types_covered_for_type(type: str):
    api_response = requests.get(base_api_url + "type/" + type)
    return [result["name"] for result in api_response.json()["damage_relations"]["double_damage_to"]]


def get_types_covered_for_type(type: str):
    return types_covered_for_type[type]


def generate_type_coverage_json():
    # store all types relations
    types_covered_for_type = {}
    for type in get_all_types():
        types_covered_for_type[type] = retrieve_types_covered_for_type(type)

    with open('type_coverage.json', 'w') as fp:
        json.dump(types_covered_for_type, fp)


types_covered_for_type = {}
with open('type_coverage.json', 'r') as fp:
    types_covered_for_type = json.load(fp)

